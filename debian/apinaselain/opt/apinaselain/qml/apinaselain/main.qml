import QtQuick 1.1
import com.nokia.meego 1.0



PageStackWindow{
    id: picview
    showStatusBar: false

    function onUpdateUrl(url)
    {
        picArea.enabled = false;
        currentPic.source = url;
    }




    //Signaali joka kertoo c++ koodille että vaihdetaan kuvaa
    signal changeUrl();




    Rectangle{
        anchors.fill: parent
        color: "black"



        Timer {
            id:time
            interval: 100; running: false; repeat: true
            onTriggered: {
                progress.text = (currentPic.progress *100).toFixed(1) + "%";
            }
        }

        Text{
            font.pointSize: 16
            color: "white"
            visible: false
            id:progress
            text: ""
            anchors.centerIn: parent

        }


        MouseArea{
            id:picArea
            anchors.fill: parent

            onClicked: {
                console.debug("Painettu");
                changeUrl();
                currentPic.opacity = 0;
            }
            /*
        PinchArea {
              id: pincharea

              property double __oldZoom

              anchors.fill: parent

              function calcZoomDelta(zoom, percent) {
                 return zoom + Math.log(percent)/Math.log(2)
              }

              onPinchStarted: {
                 __oldZoom = currentPic.zoomLevel
              }

              onPinchUpdated: {
                 currentPic.zoomLevel = calcZoomDelta(__oldZoom, pinch.scale)
              }

              onPinchFinished: {
                 currentPic.zoomLevel = calcZoomDelta(__oldZoom, pinch.scale)
              }
           }
*/


            AnimatedImage{
                id:currentPic
                width: parent.width
                height: parent.height
                source: ''  //Tähän laitetaan kuvan url
                fillMode: Image.PreserveAspectFit
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                playing: false


                Behavior on opacity { PropertyAnimation { duration: 1000 } }

                onStatusChanged:
                {
                    if(currentPic.status === Image.Ready){
                        progress.visible = false;
                        time.running = false;
                        picArea.enabled = true;
                        currentPic.playing = true;
                        currentPic.opacity = 1;
                    }
                    else if(currentPic.status === Image.Loading){
                        progress.visible = true;
                        time.running = true;
                    }

                }
            }


        }
    }
}

