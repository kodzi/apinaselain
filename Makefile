#############################################################################
# Makefile for building: apinaselain
# Generated by qmake (2.01a) (Qt 4.7.4) on: Wed Sep 26 14:35:17 2012
# Project:  apinaselain.pro
# Template: app
# Command: /home/salminen/QtSDK/Simulator/Qt/gcc/bin/qmake -spec ../../QtSDK/Simulator/Qt/gcc/mkspecs/linux-g++ -o Makefile apinaselain.pro
#############################################################################

####### Compiler, tools and options

CC            = gcc
CXX           = g++
DEFINES       = -DQT_NO_DEBUG -DQT_DECLARATIVE_LIB -DQT_GUI_LIB -DQT_NETWORK_LIB -DQT_CORE_LIB
CFLAGS        = -pipe -O2 -Wall -W -D_REENTRANT $(DEFINES)
CXXFLAGS      = -pipe -O2 -Wall -W -D_REENTRANT $(DEFINES)
INCPATH       = -I../../QtSDK/Simulator/Qt/gcc/mkspecs/linux-g++ -I. -I../../QtSDK/Simulator/Qt/gcc/include/QtCore -I../../QtSDK/Simulator/Qt/gcc/include/QtNetwork -I../../QtSDK/Simulator/Qt/gcc/include/QtGui -I../../QtSDK/Simulator/Qt/gcc/include/QtDeclarative -I../../QtSDK/Simulator/Qt/gcc/include -Iqmlapplicationviewer -I.
LINK          = g++
LFLAGS        = -Wl,-O1 -Wl,-rpath,/home/salminen/QtSDK/Simulator/Qt/gcc/lib
LIBS          = $(SUBLIBS)  -L/home/salminen/QtSDK/Simulator/Qt/gcc/lib -lQtDeclarative -lQtGui -lQtNetwork -lQtCore -lpthread 
AR            = ar cqs
RANLIB        = 
QMAKE         = /home/salminen/QtSDK/Simulator/Qt/gcc/bin/qmake
TAR           = tar -cf
COMPRESS      = gzip -9f
COPY          = cp -f
SED           = sed
COPY_FILE     = $(COPY)
COPY_DIR      = $(COPY) -r
STRIP         = strip
INSTALL_FILE  = install -m 644 -p
INSTALL_DIR   = $(COPY_DIR)
INSTALL_PROGRAM = install -m 755 -p
DEL_FILE      = rm -f
SYMLINK       = ln -f -s
DEL_DIR       = rmdir
MOVE          = mv -f
CHK_DIR_EXISTS= test -d
MKDIR         = mkdir -p

####### Output directory

OBJECTS_DIR   = ./

####### Files

SOURCES       = main.cpp \
		apina.cpp \
		qmlapplicationviewer/qmlapplicationviewer.cpp moc_qmlapplicationviewer.cpp \
		moc_apina.cpp
OBJECTS       = main.o \
		apina.o \
		qmlapplicationviewer.o \
		moc_qmlapplicationviewer.o \
		moc_apina.o
DIST          = ../../QtSDK/Simulator/Qt/gcc/mkspecs/common/g++.conf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/common/unix.conf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/common/linux.conf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/qconfig.pri \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/modules/qt_webkit_version.pri \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt_functions.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt_config.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/exclusive_builds.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/default_pre.prf \
		qmlapplicationviewer/qmlapplicationviewer.pri \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/release.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/default_post.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/warn_on.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/unix/thread.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/moc.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/resources.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/uic.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/yacc.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/lex.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/include_source_dir.prf \
		apinaselain.pro
QMAKE_TARGET  = apinaselain
DESTDIR       = 
TARGET        = apinaselain

first: all
####### Implicit rules

.SUFFIXES: .o .c .cpp .cc .cxx .C

.cpp.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cc.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.cxx.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.C.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o "$@" "$<"

.c.o:
	$(CC) -c $(CFLAGS) $(INCPATH) -o "$@" "$<"

####### Build rules

all: Makefile $(TARGET)

$(TARGET):  $(OBJECTS)  
	$(LINK) $(LFLAGS) -o $(TARGET) $(OBJECTS) $(OBJCOMP) $(LIBS)

Makefile: apinaselain.pro  ../../QtSDK/Simulator/Qt/gcc/mkspecs/linux-g++/qmake.conf ../../QtSDK/Simulator/Qt/gcc/mkspecs/common/g++.conf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/common/unix.conf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/common/linux.conf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/qconfig.pri \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/modules/qt_webkit_version.pri \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt_functions.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt_config.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/exclusive_builds.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/default_pre.prf \
		qmlapplicationviewer/qmlapplicationviewer.pri \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/release.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/default_post.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/warn_on.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/unix/thread.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/moc.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/resources.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/uic.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/yacc.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/lex.prf \
		../../QtSDK/Simulator/Qt/gcc/mkspecs/features/include_source_dir.prf
	$(QMAKE) -spec ../../QtSDK/Simulator/Qt/gcc/mkspecs/linux-g++ -o Makefile apinaselain.pro
../../QtSDK/Simulator/Qt/gcc/mkspecs/common/g++.conf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/common/unix.conf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/common/linux.conf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/qconfig.pri:
../../QtSDK/Simulator/Qt/gcc/mkspecs/modules/qt_webkit_version.pri:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt_functions.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt_config.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/exclusive_builds.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/default_pre.prf:
qmlapplicationviewer/qmlapplicationviewer.pri:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/release.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/default_post.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/warn_on.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/qt.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/unix/thread.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/moc.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/resources.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/uic.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/yacc.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/lex.prf:
../../QtSDK/Simulator/Qt/gcc/mkspecs/features/include_source_dir.prf:
qmake:  FORCE
	@$(QMAKE) -spec ../../QtSDK/Simulator/Qt/gcc/mkspecs/linux-g++ -o Makefile apinaselain.pro

dist: 
	@$(CHK_DIR_EXISTS) .tmp/apinaselain1.0.0 || $(MKDIR) .tmp/apinaselain1.0.0 
	$(COPY_FILE) --parents $(SOURCES) $(DIST) .tmp/apinaselain1.0.0/ && $(COPY_FILE) --parents qmlapplicationviewer/qmlapplicationviewer.h apina.h .tmp/apinaselain1.0.0/ && $(COPY_FILE) --parents main.cpp apina.cpp qmlapplicationviewer/qmlapplicationviewer.cpp .tmp/apinaselain1.0.0/ && (cd `dirname .tmp/apinaselain1.0.0` && $(TAR) apinaselain1.0.0.tar apinaselain1.0.0 && $(COMPRESS) apinaselain1.0.0.tar) && $(MOVE) `dirname .tmp/apinaselain1.0.0`/apinaselain1.0.0.tar.gz . && $(DEL_FILE) -r .tmp/apinaselain1.0.0


clean:compiler_clean 
	-$(DEL_FILE) $(OBJECTS)
	-$(DEL_FILE) *~ core *.core


####### Sub-libraries

distclean: clean
	-$(DEL_FILE) $(TARGET) 
	-$(DEL_FILE) Makefile


check: first

mocclean: compiler_moc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_source_make_all

compiler_moc_header_make_all: moc_qmlapplicationviewer.cpp moc_apina.cpp
compiler_moc_header_clean:
	-$(DEL_FILE) moc_qmlapplicationviewer.cpp moc_apina.cpp
moc_qmlapplicationviewer.cpp: qmlapplicationviewer/qmlapplicationviewer.h
	/home/salminen/QtSDK/Simulator/Qt/gcc/bin/moc $(DEFINES) $(INCPATH) qmlapplicationviewer/qmlapplicationviewer.h -o moc_qmlapplicationviewer.cpp

moc_apina.cpp: apina.h
	/home/salminen/QtSDK/Simulator/Qt/gcc/bin/moc $(DEFINES) $(INCPATH) apina.h -o moc_apina.cpp

compiler_rcc_make_all:
compiler_rcc_clean:
compiler_image_collection_make_all: qmake_image_collection.cpp
compiler_image_collection_clean:
	-$(DEL_FILE) qmake_image_collection.cpp
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_uic_make_all:
compiler_uic_clean:
compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: compiler_moc_header_clean 

####### Compile

main.o: main.cpp apina.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o main.o main.cpp

apina.o: apina.cpp apina.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o apina.o apina.cpp

qmlapplicationviewer.o: qmlapplicationviewer/qmlapplicationviewer.cpp qmlapplicationviewer/qmlapplicationviewer.h
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o qmlapplicationviewer.o qmlapplicationviewer/qmlapplicationviewer.cpp

moc_qmlapplicationviewer.o: moc_qmlapplicationviewer.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_qmlapplicationviewer.o moc_qmlapplicationviewer.cpp

moc_apina.o: moc_apina.cpp 
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o moc_apina.o moc_apina.cpp

####### Install

install_itemfolder_01: first FORCE
	@$(CHK_DIR_EXISTS) $(INSTALL_ROOT)/opt/apinaselain/qml/ || $(MKDIR) $(INSTALL_ROOT)/opt/apinaselain/qml/ 
	-$(INSTALL_DIR) /home/salminen/projekti/apinaselain/qml/apinaselain $(INSTALL_ROOT)/opt/apinaselain/qml/


uninstall_itemfolder_01:  FORCE
	-$(DEL_FILE) -r $(INSTALL_ROOT)/opt/apinaselain/qml/apinaselain
	-$(DEL_DIR) $(INSTALL_ROOT)/opt/apinaselain/qml/ 


install_target: first FORCE
	@$(CHK_DIR_EXISTS) $(INSTALL_ROOT)/opt/apinaselain/bin/ || $(MKDIR) $(INSTALL_ROOT)/opt/apinaselain/bin/ 
	-$(INSTALL_PROGRAM) "$(QMAKE_TARGET)" "$(INSTALL_ROOT)/opt/apinaselain/bin/$(QMAKE_TARGET)"
	-$(STRIP) "$(INSTALL_ROOT)/opt/apinaselain/bin/$(QMAKE_TARGET)"

uninstall_target:  FORCE
	-$(DEL_FILE) "$(INSTALL_ROOT)/opt/apinaselain/bin/$(QMAKE_TARGET)"
	-$(DEL_DIR) $(INSTALL_ROOT)/opt/apinaselain/bin/ 


install:  install_itemfolder_01 install_target  FORCE

uninstall: uninstall_itemfolder_01 uninstall_target   FORCE

FORCE:

