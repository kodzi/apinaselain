#ifndef APINA_H
#define APINA_H


#include <QDebug>
#include <QtCore>
#include <QVector>
#include <QObject>
#include <QtDeclarative>
#include <QDeclarativeItem>
#include <QDeclarativeContext>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkAccessManager>

//class Settings;

static const QString HOME = "http://apina.biz";
static const QString IMAGE_HOST= "http://termite.apcdn.com/";
static const int IMAGE_PER_PAGE = 30;

class Apina : public QObject
{
    Q_OBJECT
    // Q_PROPERTY(Settings *settings READ settings WRITE setSettings)
public:
    explicit Apina(QObject *parent = 0, QDeclarativeContext *ctxt=0);
    ~Apina();

    // initial all needed things to connect with apina.biz
    void getCookie();

    // get individual page from apina.biz
    void getPage(QString url);

    //Settings *settings() const;
    //void setSettings(Settings *settings);

signals:
    void gotImage(QVariant image);
    void gotCookie();
    void catalogLoaded();
    void randomLoaded();
    void commentLoaded();
    void orderModeChanged(QVariant order);
    void viewModeChanged(QString mode);

public slots:
    void replyFinished(QNetworkReply *reply);
    void onPopulateCatalog();
    void onSwitchOrderTo(QString order);
    void onSwitchViewMode(QString mode);

private:

    void parseCatalog();
    //    void parseComments();
    void parseRandom();

    QNetworkAccessManager *m_manager;
    QNetworkCookieJar *m_cookieJar;

    QNetworkReply *m_reply;

    QDeclarativeContext *m_ctxt;

    int m_current_page;
    bool hasCookie;
    QString m_order;
    QString m_mode;
    QVector<QString> m_randoms;
    //Settings *m_settings;
};

#endif // APINA_H
