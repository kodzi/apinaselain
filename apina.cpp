#include <QtNetwork/QNetworkRequest>
#include "apina.h"
#include <QDomDocument>
#include <QDomElement>
#include <QDomNodeList>
#include <QDomAttr>

Apina::Apina(QObject *parent, QDeclarativeContext *ctxt) :
    QObject(parent), m_ctxt(ctxt), m_current_page(0)//, m_settings(0)
{
    //DEFAULTS, replace with settings functions
    m_order = "id";
    hasCookie = false;
    m_mode = "catalog";

    m_manager = new QNetworkAccessManager(this);
    m_cookieJar = new QNetworkCookieJar(this);
    m_manager->setCookieJar(m_cookieJar);

    getCookie();
    connect(this, SIGNAL(gotCookie()), this, SLOT(onPopulateCatalog()));

    connect(parent, SIGNAL(populateCatalog()), this, SLOT(onPopulateCatalog()));
    connect(m_manager, SIGNAL(finished(QNetworkReply*)), SLOT(replyFinished(QNetworkReply*)));
    connect(this, SIGNAL(gotImage(QVariant)), parent, SLOT(onGotImage(QVariant)));

    //Catalog order
    connect(parent, SIGNAL(switchOrderTo(QString)), this, SLOT(onSwitchOrderTo(QString)));
    connect(this, SIGNAL(orderModeChanged(QVariant)), parent, SLOT(onOrderingChanged(QVariant)));

    //Viewmode: catalog or random
    connect(parent, SIGNAL(switchViewMode(QString)), this, SLOT(onSwitchViewMode(QString)));

    //Random images
    connect(this, SIGNAL(randomLoaded()), parent, SLOT(onRandomLoaded()));
}


Apina::~Apina()
{
    m_randoms.clear();
}


void Apina::getCookie()
{
    //qDebug() << "start()";
    getPage(HOME + "/?i_really=need_it");
}



void Apina::getPage(QString url){

    //qDebug() << "getPage() with " + url;
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    //QList<QNetworkCookie> cookies = m_cookieJar->cookiesForUrl(QUrl(".apina.biz"));
    m_reply = m_manager->get(request);
}
/*
Settings *Apina::settings() const
{
    return m_settings;
}

void Apina::setSettings(Settings *settings)
{
    m_settings = settings;
}*/

void Apina::replyFinished(QNetworkReply *reply)
{
    if (reply->error() != QNetworkReply::NoError){
        qDebug() << "ERROR:" << reply->errorString();
        return;
    }
    if(!hasCookie)
    {
        hasCookie = true;
        emit gotCookie();
    }
    else if(m_mode == "catalog")
    {
        parseCatalog();
    }
    else
    {
        int status = m_reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
        if(status == 302)
        {
            //qDebug() << "Redirected";
            QString redir = m_reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toString();
            getPage(HOME+redir);
        }
        parseRandom();
    }
}

void Apina::onPopulateCatalog()
{


    QString newUrl("");
    if(m_mode == "catalog")
    {
        QString page = QString::number(m_current_page*IMAGE_PER_PAGE);
        newUrl = (HOME + "/tag/*/" + page + "/" + m_order);
        m_current_page += 1;
    }
    else if(m_mode == "random")
    {
        newUrl = HOME + "/random";
    }
    getPage(newUrl);
}

void Apina::parseCatalog()
{
    //qDebug() << "parseCatalog()";

    QDomDocument doc;
    doc.setContent(m_reply->readAll(),false,0,0);

    //get all Divs
    QDomNodeList divNodeList = doc.elementsByTagName("div");

    for(int i = 0; i < divNodeList.size(); ++i)
    {
        QDomNode domNode = divNodeList.at(i);
        QDomAttr attribute = domNode.toElement().attributeNode("class");

        //qDebug() << "Attribute value" << attribute.value();
        if (attribute.value() == "list")
        {
            //qDebug() << "Lista löytyi, lapsia yhteensä: " << divNodeList.at(i).childNodes().count();
            //qDebug() << "Lapsenlapsia per, node: " << domNode.childNodes().at(i).toElement().childNodes().count();
            for (int y = 0; y < domNode.childNodes().count(); ++y)
            {
                QDomNode childNode = domNode.childNodes().at(y).childNodes().at(0).childNodes().at(0);
                QVariant content = childNode.toElement().attribute("alt");
                //qDebug() << "Kuvan id: " << content;
                emit gotImage(content);
            }
        }
    }
}

void Apina::parseRandom()
{
   QString temp = m_reply->readAll();
   QString key = "http://termite.apcdn.com/thumbs/t_";
   QString key2 = "\"";
   int index = 0;
   int index2 = 0;
    for(int i = 0; i < 8; ++i)
    {
        index = temp.indexOf(key, index + 1, Qt::CaseInsensitive );
        index2 = temp.indexOf(key2, index + key.size(), Qt::CaseInsensitive );
        index += key2.size();

        QString content(temp.mid(index, index2-index));

        if(content != "")
        {
            emit gotImage(QVariant(content.section("_",-1,-1)));

        }

    }

    emit randomLoaded();
}

void Apina::onSwitchOrderTo(QString order)
{
    //qDebug() << "onSwitchOrderTo: " << order;
    if(order != m_order)
    {
        if( order == "id")
        {
            //qDebug() << "Switch to ID-mode";

            m_order = "id";
            emit orderModeChanged(m_order);
            m_current_page = 0;
            //onPopulateCatalog();
        }
        else if( order == "rating")
        {
            //qDebug() << "Switch to RATING-mode";

            m_order = "rating";
            emit orderModeChanged(m_order);
            m_current_page = 0;
            //onPopulateCatalog();
        }
        else
        {
            qDebug() << "Mode not recognised";
        }
    }
}

void Apina::onSwitchViewMode(QString mode)
{
    if(m_mode != mode)
    {
        m_mode = mode;
    }
    onPopulateCatalog();
}
