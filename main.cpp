#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"
#include "apina.h"
#include <QDeclarativeContext>
#include "swipecontrol.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    QScopedPointer<QApplication> app(createApplication(argc, argv));
    QmlApplicationViewer viewer;

    app->setOrganizationName("ApinaSelain");
    app->setApplicationName("ApinaSelain");

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.setMainQmlFile(QLatin1String("qml/apinaselain/main.qml"));


    SwipeControl * swipeControl = new SwipeControl(&viewer, true);

    QObject *rootObject = dynamic_cast<QObject*>(viewer.rootObject());
    QDeclarativeContext *ctxt = viewer.rootContext();
    Apina *api = new Apina(rootObject, ctxt);



    viewer.showExpanded();

    return app->exec();
    delete api;
    api = 0;
}
