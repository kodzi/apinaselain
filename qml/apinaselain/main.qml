import QtQuick 1.1
import com.nokia.meego 1.0
SwipePageStackWindow {
    id: appWindow
    property string viewMode: "catalog"
    property string orderMode: "id"
    property bool darkMode: true
    property string image_host: "http://termite.apcdn.com"

    style: PageStackWindowStyle{cornersVisible: false}

    showStatusBar: inPortrait
    initialPage: mainPage

    //populate catalog and random according to viewMode
    signal populateCatalog();


    signal switchOrderTo(string order);
    signal switchViewMode(string mode);

    function invertTheme()
    {
        theme.inverted = !theme.inverted
        darkMode = !darkMode
    }
    function onGotImage(image)
    {
        if(viewMode === "catalog")
        {
            catalog.append({name: image})
            //console.log(catalog.count)
        }
        else
        {
            //console.log(image)
            random.append({name: image})
            //console.log(random.count)
        }
        //console.log(catalog.count)
    }
    function hideStatusBar()
    {
        showSB = !showSB
    }
    function onRandomLoaded()
    {
        imagePage.viewModel = random;
    }

    function onOrderingChanged(order)
    {
        //console.log("New ordering: " + order)
        if(String(order) === orderMode)
        {
            catalog.clear();
        }
        else
        {
            console.log("Conflic in setting orderMode");
        }
    }


    MainPage{
        id: mainPage
    }
    ImagePage{
        id: imagePage
    }
    ListModel{
        id: catalog
    }

    ListModel{
        id: random
    }


    QueryDialog {
        id: aboutDialog
        titleText: "SwipePageStackWindow"
        message: "(C) [2013] [AlejoSotelo.com.ar]\n[0.1.0]"
    }

    Component.onCompleted: {
        invertTheme()
    }
}

