import QtQuick 1.1
import com.nokia.meego 1.0

Item{
    id: rootZoomableImage
    property bool allowDelegateFlicking: photo.status === Image.Ready && photo.scale > pinchArea.minScale
    width: (inPortrait)? 480:854
    //height: (inPortrait)? 854-35:480
    height: parent.height
    signal clicked()
    signal longPress()


    Flickable {
        id: flickable
        clip: true
        anchors.fill: parent
        contentWidth: imageContainer.width
        contentHeight: imageContainer.height
        onHeightChanged: photo.calculateSize()


        Item {
            id: imageContainer
            width: Math.max(photo.width * photo.scale, flickable.width)
            height: Math.max(photo.height * photo.scale, flickable.height)

            AnimatedImage {
                id: photo
                property real prevScale
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                smooth: !flickable.movingVertically
                fillMode: Image.PreserveAspectFit
                playing: false
                source: image_host + "/full/" + model.name
                function calculateSize()
                {
                    scale = Math.min(flickable.width / width, flickable.height / height) * 0.98;
                    pinchArea.minScale = scale;
                    prevScale = Math.min(scale, 1);
                }


                onStatusChanged: {
                    if (photo.status === Image.Loading) {

                    } else if (photo.status === Image.Error ){
                        console.log("Error while loading an image ");
                    }
                    else if (photo.status === Image.Ready) {
                        photo.playing = true
                        photo.smooth = true
                        photo.visible = true
                        calculateSize();
                    }
                }

                onScaleChanged: {

                    if ((width * scale) > flickable.width) {
                        var xoff = (flickable.width / 2 + flickable.contentX) * scale / prevScale;
                        flickable.contentX = xoff - flickable.width / 2;
                    }
                    if ((height * scale) > flickable.height) {
                        var yoff = (flickable.height / 2 + flickable.contentY) * scale / prevScale;
                        flickable.contentY = yoff - flickable.height / 2;
                    }

                    prevScale = scale;

                }
            }


        }


        PinchArea{
            id: pinchArea
            property real minScale:  1.0
            anchors.fill: parent
            property real lastScale: 1.0
            pinch.target: photo
            pinch.minimumScale: minScale
            pinch.maximumScale: 3.0

            onPinchFinished: flickable.returnToBounds()
        }


        MouseArea {
            id: mousearea
            anchors.fill : parent
            property bool doubleClicked:  false

            Timer{
                id: clickTimer
                interval: 520
                onTriggered: rootZoomableImage.clicked()
                running: false
                repeat:  false
            }

            onDoubleClicked: {
                clickTimer.stop();
                mouse.accepted = true;
                console.log("Double clicked");
                if ( photo.scale > pinchArea.minScale){
                    photo.scale = pinchArea.minScale;
                    flickable.returnToBounds();
                    //console.log("photo.scale: "+photo.scale)
                }else{
                    photo.scale = 1.0
                }
            }

            onClicked: {
                console.log("clicked")
                clickTimer.start()
            }
        }

    }



    Loader {
        id: busyLoader
        sourceComponent: (photo.status === Image.Loading) ? updatingIndicator : undefined
        visible: (photo.status === Image.Loading)
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        Component {
            id: updatingIndicator;
            BusyIndicator{
                id: busyIndicator
                BusyIndicatorStyle { size: "large" }
                running: busyLoader.visible
            }
        }
    }

    ProgressBar{
        id: progressBar
        width: 320
        minimumValue: 0.0
        maximumValue: 1.0
        value: photo.progress
        visible: busyLoader.visible
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
    }

    ScrollDecorator{flickableItem: flickable}
}

