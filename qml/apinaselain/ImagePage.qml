import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1
Page {

    property real cIndex: -1
    property real rIndex: -1
    property alias viewModel: imageView.model

    function showBanner(bannerText){
        banner.timerShowTime = 1500 // ms
        banner.text = bannerText
        banner.show();
    }

    ListView
    {
        id: imageView
        anchors.fill: parent
        delegate: ImageDelegate{ id: imageDelegate}
        boundsBehavior: Flickable.DragOverBounds
        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange

        onCurrentIndexChanged: {
            //console.log("ImagePage " + currentIndex + " index")
            if( viewMode !== "catalog")
            {
                rIndex = currentIndex;
            }
        }

        property bool __wasAtXEnd: false
        property int __initialContentX: 0
        property bool __toBeRefresh: false

        onMovementStarted: {
            __wasAtXEnd = atXEnd
            __initialContentX = contentX
        }
        onContentXChanged: {
            if (__wasAtXEnd
                    && __initialContentX - contentX < 50)
                __toBeRefresh = true
        }
        onMovementEnded: {
            if (__toBeRefresh) {
                showBanner("Loading more images...");
                //viewMode = "random";
                switchViewMode(viewMode);
                __toBeRefresh = false
            }
        }

    }

    Connections{
        target: imagePage.status === PageStatus.Active ? appWindow : null // Por si se agrega otro Item al pageStack
        ignoreUnknownSignals: true
        onRightSwipe: {
            console.debug("rightSwipe");
            //pageStack.pop();
        }
        onClicked:{
            if(type === "catalog")
            {
                viewMode = "catalog"
                pageStack.pop();
            }
            else if(type === "about")
                aboutDialog.open();
            else if(type === "nightMode")
            {
                appWindow.menuModel.set(index,{title: darkMode? "Bright mode":"Night mode", type:"nightMode", iconSource: "image://theme/icon-m-toolbar-home-white"});
                invertTheme();
            }
        }
    }
    InfoBanner {
        id: banner
    }


    ToolIcon
    {
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        iconId: "toolbar-grid"
    }
    onStatusChanged: {
        if(status === PageStatus.Activating)
        {
            imageView.positionViewAtIndex((viewMode === "catalog")? cIndex:rIndex, ListView.Beginning)

            appWindow.menuModel.clear();
            appWindow.menuModel.append({title: darkMode? "Bright mode":"Night mode", type:"nightMode", iconSource: "image://theme/icon-m-toolbar-clock-white"});
            appWindow.menuModel.append({title: "Catalog", type:"catalog", iconSource: "image://theme/icon-m-toolbar-home-white"});
            appWindow.menuModel.append({title: "About", type:"about", iconSource: "image://theme/icon-m-toolbar-view-menu-white"});
        }
    }



}
