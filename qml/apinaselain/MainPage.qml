import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.1
Page {
    id: mainPage
    property int pagenumber
    property int images_per_page
    property alias model: gridView.model
    property int imageWidth: 160
    orientationLock: PageOrientation.LockPortrait
    function showBanner(bannerText){
        banner.timerShowTime = 1500 // ms
        banner.text = bannerText
        banner.show();
    }

    Component {
        id: catDelegate
        Item {
            id: catImage
            width: gridView.cellWidth; height: gridView.cellHeight
            Image{
                source: "http://termite.apcdn.com/thumbs/t_" + name
                id: img
                width: imageWidth - 2
                height: imageWidth - 2
                //playing: mainPage.status
                anchors.centerIn: parent
                fillMode: Image.PreserveAspectCrop
                clip: true
                onStatusChanged: {
                    if (img.status === Image.Loading) {
                        img.visible = false;
                    } else if (img.status === Image.Error ){
                        console.log("Error while loading an image ");
                    }
                    else if (img.status === Image.Ready) {
                        //img.playing = mainPage.status

                        img.smooth = true
                        img.visible = true
                    }
                }
            }
            Image{
                id: gifImg
                source: "qrc:///images/icon-gif.png"
                anchors.centerIn: catImage
                visible: img.visible ? (name.slice(-3) === "gif"):false

            }
            BusyIndicator{
                id: busyIndicator
                anchors.centerIn: parent
                BusyIndicatorStyle { size: "medium" }
                running: (img.status === Image.Loading)
                visible: (img.status === Image.Loading)
            }
            MouseArea{
                id: mouseArea
                anchors.fill: parent
                onClicked:{
                    //console.log("Load fullsize of index " + index + " name " + name)
                    imagePage.cIndex = index
                    viewMode = "catalog"
                    imagePage.viewModel = gridView.model
                    appWindow.pageStack.push(imagePage)
                }

            }

        }

    }

    Connections{
        target: status === PageStatus.Active ? appWindow : null // Por si se agrega otro Item al pageStack
        ignoreUnknownSignals: true
        onClicked:{
            if(type === "about")
                aboutDialog.open();
            else if(type === "random")
            {
                viewMode = "random";
                switchViewMode(viewMode);
                appWindow.pageStack.push(imagePage);

            }
            else if(type === "nightMode")
            {
                appWindow.menuModel.set(index,{title: darkMode? "Bright mode":"Night mode", type:"nightMode", iconSource: "image://theme/icon-m-toolbar-home-white"});
                invertTheme();
            }
            else if(type === "switchOrder")
            {
                if(orderMode === "id")
                {
                    orderMode = "rating";
                }else
                {
                    orderMode = "id"
                }
                switchOrderTo(orderMode);
                viewMode = "catalog";
                switchViewMode(viewMode);
                appWindow.menuModel.set(index, {title: "Ordering by: " + orderMode, type:"switchOrder", iconSource: "image://theme/icon-m-toolbar-add-white"})
            }
        }
    }

    GridView{
        id: gridView
        anchors.fill: parent
        //interactive: catalog.count === 0 ? false:true
        cellWidth: imageWidth; cellHeight: imageWidth
        model: catalog
        delegate: catDelegate
        focus: true

        //Src: http://dickson-dev.blogspot.fi/2012/12/how-to-create-pull-down-listview-in-qml.html
        property bool __wasAtYEnd: false
        property int __initialContentY: 0
        property bool __toBeRefresh: false

        onMovementStarted: {
            __wasAtYEnd = atYEnd
            __initialContentY = contentY
        }
        onContentYChanged: {
            if (__wasAtYEnd
                    && __initialContentY - contentY < 100)
                __toBeRefresh = true
        }
        onMovementEnded: {
            if (__toBeRefresh) {
                showBanner("Loading next page...")
                viewMode = "catalog";
                switchViewMode(viewMode)
                __toBeRefresh = false
            }
        }

    }

    ScrollDecorator{
        flickableItem: gridView
    }
    InfoBanner {
        id: banner
    }
    onStatusChanged: {

        if(status === PageStatus.Activating)
        {

            appWindow.menuModel.clear();
            appWindow.menuModel.append({title: darkMode? "Bright mode":"Night mode", type:"nightMode", iconSource: "image://theme/icon-m-toolbar-clock-white"});
            appWindow.menuModel.append({title: "Ordering by: " + orderMode, type:"switchOrder", iconSource: "image://theme/icon-m-toolbar-repeat-white"});
            appWindow.menuModel.append({title: "Random", type:"random", iconSource: "image://theme/icon-m-toolbar-shuffle-white"});
            appWindow.menuModel.append({title: "About", type:"about", iconSource: "image://theme/icon-m-toolbar-view-menu-white"});
        }
    }
}
